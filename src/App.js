import React, { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import './App.css';

import CatalogImages from './CatalogImages';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  state = { users: [], files:[], columns: 3 }

  componentDidMount() {
    fetch('/users')
      .then(res => res.json())
      .then(users => this.setState({ users }));

    fetch( '/files' )
      .then(res => res.json())
      .then(files => this.setState({ files }));

  }

  render() {

    const niklas = [
      1,
      65,
      68,
      130,
      140,
      146,
      160,
      165,
      195,
      198,
      199,
      200,
      204,
      256,
      263,
      266,
      273,
      278,
      281,
      282,
      283,
      285,
      287,
      291,
      298,
      302,
      306,
      316,
      318,
      319,
      321,
      322,
      324,
      329,
      344,
      348,
      349,
      360,
      368,
      367,
    ]
  
    const maya = [
      4, 11, 28, 50, 51, 53, 54 , 78, 85, 126, 129, 138, 139, 143, 144, 145, 146 ,164,174, 175, 176, 179, 195, 202, 209, 210, 211, 213, 216, 219, 221, 222, 223, 224, 228, 230, 231, 232, 233, 240, 249, 254, 256, 257, 264, 272, 273, 283, 282, 276, 284, 285, 298, 307, 322, 344
    ]
  
    const kirstin = [
      3,
      4,
      22,
      54,
      55,
      56,
      57,
      58,
      64,
      71,
      92,
      101,
      117,
      138,
      146,
      231,
      282,
      283,
      284,
      291,
      319,
      320,
      321,
      348,
      350,
      351,
      358,
      360,
      367,
      369,
      371,
    ]
  
    const silke = [
      26, 27, 52, 70, 86, 107, 149, 164, 41, 62, 71, 74, 167, 174, 181, 185, 193, 195, 125, 199, 243, 257, 258, 260, 266, 200, 202, 204, 206, 207, 209, 211, 223, 225, 228, 229, 231, 232, 240, 271, 291, 298, 307, 351, 358, 360, 362, 369
    ]
  
    const sven = [
      1,
      3,
      9,
      11,
      13,
      14,
      16,
      18,
      19,
      22,
      23,
      30,
      25,
      26,
      27,
      41,
      43,
      46,
      48,
      58,
      59,
      66,
      70,
      71,
      72,
      74,
      78,
      84,
      87,
      102,
      122,
      124,
      131,
      142,
      149,
      165,
      173,
      181,
      193,
      200,
      202,
      204,
      205,
      214,
      217,
      220,
      222,
      223,
      224,
      231,
      244,
      57,
      269,
      272,
      280,
      282,
      283,
      286,
      296,
      301,
      307,
      309,
      310,
      318,
      320,
      326,
      328,
      329,
      335,
      336,
      345,
      347,
      349,
      352,
      353,
      359,
      362,
      368,
    ]

    const images = {
      firsts: [],
      seconds: [],
    };

    console.log( this.state.files );

    this.state.files.forEach( ( file, idx ) => {
      if ( idx % 2 === 1 ) {
        images.firsts.push( file );
      } else {
        images.seconds.push( file );
      }
    } );
    return (
      <div className="App">
        <div class="intro">
          <h1>Gudrun Jork</h1>
          <h2>Katalog</h2>
        </div>
        <div className="numbers">
        <div>kirstin: { kirstin.length }</div>
          <div>niklas: { niklas.length }</div>
          <div>silke: { silke.length }</div>
          <div>maya: { maya.length }</div>
          <div>sven: { sven.length }</div>
        </div>
        <div className={ `catalog columns-${ this.state.columns}` }>
          <CatalogImages images={ images } />
        </div>
        <div className="controls">
          <span>Spalten:</span>
          <ButtonGroup size="lg">
            <Button variant="outline-dark" active={ this.state.columns === 1 } onClick={ () => this.setState( { columns: 1 } ) }>1</Button>
            <Button variant="outline-dark" active={ this.state.columns === 2 } onClick={ () => this.setState( { columns: 2 } ) }>2</Button>
            <Button variant="outline-dark" active={ this.state.columns === 3 } onClick={ () => this.setState( { columns: 3 } ) }>3</Button>
            <Button variant="outline-dark" active={ this.state.columns === 4 } onClick={ () => this.setState( { columns: 4 } ) }>4</Button>
            <Button variant="outline-dark" active={ this.state.columns === 5 } onClick={ () => this.setState( { columns: 5 } ) }>5</Button>
            <Button variant="outline-dark" active={ this.state.columns === 6 } onClick={ () => this.setState( { columns: 6 } ) }>6</Button>
          </ButtonGroup>
        </div>
      </div>
    );
  }
}

export default App;
